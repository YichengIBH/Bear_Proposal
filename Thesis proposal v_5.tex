\documentclass[12pt]{article}
 
\usepackage[margin=1in]{geometry} 
\usepackage{amsmath,amsthm,amssymb}
\usepackage{graphicx}
\usepackage[english]{babel}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\usepackage{blindtext, xcolor}
\usepackage{comment} 
\newenvironment{theorem}[2][Theorem]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{lemma}[2][Lemma]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{exercise}[2][Exercise]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{reflection}[2][Reflection]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{proposition}[2][Proposition]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{corollary}[2][Corollary]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newcommand{\COMMENT}[1]{\textcolor{green}{(#1)}}
 
\begin{document}
 
% --------------------------------------------------------------
%                         Start here
% --------------------------------------------------------------
 
%\renewcommand{\qedsymbol}{\filledbox}
 
\title{On the tidal current and wave induced sediment transport at the Old inlet breach, Fire island}%replace X with the appropriate number
\author{Yicheng Huang\\ %replace with your name
} %if necessary, replace with your course title
 
\maketitle 
\section{Introduction}
%Raise the basic general question and the gap of knowledge, and review the methods to study the sediment transport at tidal inlet region. (100)
The tidal inlet is a region where the local morphodynamics acts vigorously. The geological time scale at a tidal inlet will be in the order of years or decades \cite{van2017tidal}. It is also a region where the local hydrodynamic environment is complicated. The tidal current will carry strong nonlinearity. The surface wave will interact with the bathymetry and the tidal current. Such situation makes the prediction of sediment transport at tidal inlet difficult. Although many successful numerical simulations have been run in such region \cite{olabarrieta2011wave,dodet2013wave,chen2015hydrodynamic}, up to now, people still have not understood clearly about the mechanism of sediment mobility at the tidal inlet.\\

\begin{figure}
  \centerline{\includegraphics[scale=1]{ChalieP}}
  \caption{Selected aerial photos took by Dr. Charles Flagg over the Old Inlet breach.}
  \label{Aerial}
\end{figure}

%Concerning the Old Inlet Breach. (200)
The Old Inlet Breach at Fire Island, New York, is a tidal inlet opened in 2012 by Hurricane Sandy \cite{lisa2016on}. Since then, seawater enters through the breach into the eastern Great South Bay, profoundly changing the bay circulation \cite{hinrichs2018assessment}.  A serial of aerial photographs has been taken by Dr. Charles Flagg from 2013 to 2018 every month to keep track of the inlet morphological alteration (selected images are shown in figure \ref{Aerial}). The most intrigues character of the flood shoal is the petal-like sediment distribution pattern. As the sediment accumulates, the pattern will continuously change their shape and extension. In five years, the west edge of the flood shoal near the west bank has extended from $72.905^{\circ}W$ to $72.910^{\circ}W$. From these pictures, one of the primary sediment sources is the growth and break of the sand spit on the west bank, the other source is the occasional sand plume injected through the main-channel into the back bay. During the first three years after inlet opening (2012 to 2015), the main channel continuously deepens and widens despite the sediment input. However, in recent three years (2016 to 2018), some sand reef has been built up, which might clog the main channel. And a tidal-flat grows at the east bank, that narrows the gorge of the inlet. All of these factors indicate that, with continuous sediment input, the Old Inlet Breach is at risk of clogging. In this way, finding out the mechanism of such sediment flux can help us understand the inlet closing processes. Moreover, investigating the local source and sink of the sediment transport, and clarifying their hydrodynamic cause can help us further understand wave and tidal current's role in the morphodynamics of a tidal inlet.\\

% Propose the research (200)
The proposed research will focus on three primary points: to understand the mechanism of the total sediment transport through the main channel, to understand the mechanism of local sediment transport divergence or convergence, and to model the sediment transport during a storm event. In our research, we will refine the existing hydraulic model \cite{yu2018hydraulic} for analyzing the total sediment transport; and establish a wave-meanflow interactive hydrodynamic model for analyzing the local sediment transport.


\section{Background}
%The Soulsby-Van-Rijn formula. (100)
To estimate the local sediment transport flux, we take advantage of the famous Soulsby-Van Rijn formula \cite{soulsby1997dynamics}, which can relate sediment transport with the local wave climate and current strength:

\begin{align}
q_{t}=A_{s}\overline{U}\Big[\Big(\overline{U}^{2}+\frac{0.018}{C_{D}}U_{rms}^{2}\Big)^{1/2}-\overline{U}_{cr}\Big]^{2.4}(1-1.6\tan\beta),
\label{SVRformula}
\end{align}

\noindent where $\overline{U}$ is the depth-averaged current velocity, which can be spatially and temporally varying. $U_{rms}$ is the root-mean-square wave orbital velocity. $\overline{U}_{cr}$ is the threshold current velocity. $\tan\beta$ is the bottom slope. $C_{D}$ is the current drag coefficient. $A_{s}$ is an empirical constant coefficient that related to the sediment size distribution. $q_{t}$ is the sediment transport flux with a unit of $m^{2}/s$. Equation \ref{SVRformula} gives us an easier way to treat sediment transport by just knowing the hydrodynamic information.\\

%some connection that links the hydraulic part with the hydrodynamic part
The hydrodynamics at the inlet region can be investigated in two different ways. One way is through a set of hydraulic equations, which might be solved analytically \cite{keulegan1967tidal,mehta1978inlet}. The other way is through the wave-current coupled hydrodynamic model. Model with realistic bathymetry requires sophisticated numerical simulation \cite{chen2015hydrodynamic,dodet2013wave,olabarrieta2011wave,olabarrieta2014role}, but for some idealized current configuration, analytical result has been developed \cite{longuet1960changes,longuet1961changes}.\\

%The hydraulic equation and its physics. Considering the integrated physical picture. (200)
The hydraulic equation, which is often used by engineers, simplify the ocean-inlet-bay system into a communicating vessel. As the tide flush back and forth inside the system, it can be viewed as a forced oscillator with nonlinear damping \cite{te1959open}, so that the mean transport through the channel can be expressed in an ordinary differential equation set. Following \cite{van2017tidal}, we can describe a one inlet system by a dynamical equation

\begin{align}
\frac{du}{dt}+\Big(\frac{mL}{2}+\frac{FL^{2}}{(h+\eta_{m})}\Big)u|u|=g\frac{\eta_{0}-\eta_{b}}{L},
\label{Hydroeqn1}
\end{align}

\noindent in which $b$ is the inlet width, $L$ is the inlet length, $h$ is a mean depth, $g$ is gravity acceleration, $u$ is the cross-sectional averaged velocity, $t$ is the time. The coefficient $F$ is a friction factor, and $m$ is energy correction coefficient (see in section \ref{Corisec}). The ocean tide $\eta_{0}$ forces the system, $\eta_{b}$ is the basin tide elevation, $\eta_{m}=(\eta_{0}+\eta_{b})/2$ is the representative water level for the inlet. Since there is another variable $\eta_{b}$, the equation set should be closed by a continuity equation:

\begin{align}
b(h+\eta_{m})u=A_{b}\frac{d\eta_{b}}{dt}.
\label{Hydroeqn2}
\end{align}

\noindent Here, $A_{b}$ is the basin surface area. Clearly, from (\ref{Hydroeqn1}) the water motion inside the inlet is driven by the pressure difference of ocean and back bay and dissipated by varies kinds of ways. Such hydraulic equations will not give the exact prediction about the details of fluid fields, but it can provide a reasonable prediction about the average volume transport variation if all the parameters are appropriately estimated. Also, it effectively depicts the ocean-inlet-bay system, so that it can be easily generalized into the case with multi-inlet \cite{yu2018hydraulic,brouwer2007effects}.\\

%M4 tide will contribute to the unidirectional sediment transport. (200)
In equation (\ref{Hydroeqn1}), even if the $\eta_{0}$ contains only the semi-diurnal constituent $\eta_{M2}$, the system will automatically generate an overtide because of the variation of the tidal stage and quadratic friction \cite{boon1981basin,fry1987tidal}. In the meanwhile, $\eta_{0}$ will usually contain $\eta_{M4}$ by itself, because of off-shore nonlinear tide dynamics \cite{pingree_griffiths_1979}. At the Old Inlet Breach, it is found that $M4$ constituent takes about $15\%$ of the principal tidal amplitude \cite{yu2018hydraulic}. So, $M4$ tide will be an essential part of forcing, and the solution to equation (\ref{Hydroeqn1}) and (\ref{Hydroeqn2}) will carry the form of:

\begin{align}
u=\hat{u}_{M2}\sin(\sigma t+\phi_{M2}^{C})+\hat{u}_{M4}\sin(2\sigma t+\phi_{M4}^{C}).
\end{align} 

\noindent Here, $\hat{u}_{M2}$ is the M2 amplitude of current velocity, $\hat{u}_{M4}$ is the M4 amplitude of current velocity, $\phi_{M2}^{C}$ and $\phi_{M4}^{C}$ are the phases for M2 and M4 constituents of current velocity, respectively. And $\sigma$ is the M2 tide frequency. According to equation (\ref{Hydroeqn1}), when $\eta_{0}>\eta_{b}$ the pressure gradient force will be positive and drives a positive $u$ into the back bay. So, the positive $u$ is pointing at flooding direction.\\

Although the cross-sectional averaged tidal current velocity $u$ can be expressed by a linear combination of two harmonic constituents, as the sediment transport can be scaled by the cubic of current velocity: $q\sim u^{3}$ \cite{nielsen1992coastal}, the time averaged sediment transport $\overline{q}$ is not zero. In fact, it can be shown that, the sediment transport can be scaled by:

\begin{align}
\overline{q}\sim \overline{u^{3}}=\frac{3}{4}\hat{u}_{M2}^{2}\hat{u}_{M4}\sin(2\phi_{M2}^{C}-\phi_{M4}^{C})=\frac{3}{4}\hat{u}_{M2}^{2}\hat{u}_{M2}\sin\delta\phi_{M2/M4}^{C},
\label{STesti}
\end{align}

\noindent In this way, the direction and strength of unidirectional sediment transport are deeply related to the M2-M4 tide interaction, which is determined by the relative phase between $M2$ and $M4$: $\delta\phi_{M2/M4}^{C}=2\phi_{M2}^{C}-\phi_{M4}^{C}$ (see in section \ref{M2M4intersec}). This means that a flood dominate asymmetric system ($\sin\delta\phi_{M2/M4}^{C}>0$) will generate a net sediment transport at flood direction; an ebb dominate asymmetric system ($\sin\delta\phi_{M2/M4}^{C}<0$) will generate a net sediment transport at ebb direction.\\

%The numerical validation of the M2-M4 interaction (200)
From an FVCOM model \cite{hinrichs2018assessment} result, the M2-M4 interaction at the tidal inlet region is significant. We took the depth averaged current velocity time series from an observational station at the inlet gorge. The spectrum of that time series is shown in figure (\ref{Spec}). The velocity amplitude ratio $u_{M4}/u_{M2}$ is $0.14$, which agree with the estimation in previous literature \cite{yu2018hydraulic}. And the relative phase $\delta\phi_{M2/M4}^{C}$ is $168^{\circ}$. This result indicates that the M2-M4 tide interaction at the Old Inlet Breach is significant, and will generate a mean sediment transport at flooding direction. This quick check agrees with the observation which shows the sediment is continuously transported from the ocean side into the back bay (see in figure \ref{Aerial}). Such sediment transport mechanism have been found and studied in other region \cite{van1993tide,pingree_griffiths_1979}. Nevertheless, several other factors will also contribute to the phenomenon.\\

\begin{figure}
  \centerline{\includegraphics[scale=0.7]{Spectrum}}
  \caption{The upper panel shows the harmonic spectrum of the $|U|$ and $\eta$ time series extracted from the FVCOM model at the Old Inlet Breach gorge; the lower panel shows the corresponding phase.}
  \label{Spec}
\end{figure}

%Mean transport (100)
One alternative factor is contributed by the mean volume transport. Without taking the nonlinearity of sediment transport into consideration, the quadratic friction and tidal stage variation effects inside the equation (\ref{Hydroeqn1}) and (\ref{Hydroeqn2}) will generate unidirectional volume transport by itself \cite{boon1981basin}. Such mean current is also captured by the FVCOM model \cite{hinrichs2018assessment}. In figure \ref{Spec} we can find a mean current velocity at $0.12m/s$. It is Stokes transport with Eulerian transport together generate such mean volume transport \cite{van2017tidal}. At the Old Inlet Breach, the mean volume transport can be up to $61m^{3}/s$ \cite{hinrichs2018assessment}. Such mean water mass transport can produce a mean sediment transport directly, and its direction is also pointing into the bay.\\

%Wave will contribute to the unidirectional sediment transport. (100)
The other aspect, which cannot be covered by the hydraulic model, and was not captured in the FVCOM model build in \cite{hinrichs2018assessment} is the surface gravity wave. The wave will stir up the sediment and increase their mobility \cite{soulsby1997dynamics}. In the Van Rijn formula (\ref{SVRformula}), such effect is captured by adding a term proportional to the wave energy: $\frac{0.018}{C_{D}}U_{rms}^{2}$. So, if wave grows, this term increases, and $q_{t}$ will increase. From a recent simulation \cite{dodet2013wave}, it is found that because of wave-meanflow interaction the wave energy is different for the flooding and ebbing phase. Such asymmetry of wave energy during a tidal cycle will significantly decrease the seaward residue sediment transport. So, a current-wave coupled sediment transport model needs to be considered at the tidal inlet region.\\


%The Longuet-Higgins wave current interaction result. Considering the local current structure. (200)
For all the points mentioned above, the sediment transport through the inlet is studied in a spatially averaged way, which cannot link the local sediment transport with the current structure. To further understand the morphodynamics at the inlet region, we need to specify the relation between the local current structure with sediment accretion or erosion. However, the strong wave-meanflow-bathymetry interaction at the tidal inlet region makes question more difficult than just evaluate the local current gradient asymmetry. \cite{longuet1960changes,longuet1961changes} offers a method to attack the problem, they asserted in their paper that, the current gradient would squeeze or flatten the wave. Moreover, by a natural generalization of their work, we can find that the bathymetry gradient will act similarly. Such wave squeezing and stretching effect can be described by the change of wave number:

\begin{align}
\frac{\partial k}{\partial x}=-\frac{k}{\sqrt{gh}+U}\Big(\frac{\partial U}{\partial x}+\frac{\partial \sqrt{gh}}{\partial x}\Big),
\label{WS1}
\end{align}

\noindent and the change of wave energy:

\begin{align}
\frac{\partial E}{\partial x}=-\frac{E}{\sqrt{gh}+U}\Big(\frac{7}{4}\frac{\partial U}{\partial x}+\frac{\partial \sqrt{gh}}{\partial x}\Big).
\label{WS2}
\end{align}

\noindent In equation (\ref{WS1}) and (\ref{WS2}), a positive gradient of velocity or depth will produce a negative gradient of wave number, which means that the wavelength is getting longer. Meanwhile, a positive gradient of velocity and depth will produce an energy divergence, which means the significant wave height is decreasing. This indicates that the current gradient and bathymetry gradient are stretching or flattening the wave. On the other hand, if we have a negative current gradient and depth gradient, (\ref{WS1}) and (\ref{WS2}) will tell an opposite story, in such situation, the wave is being squeezed or steepened. Such wave squeezing and stretching phenomenon can affect the sediment transport by changing bottom orbital velocity. According to (\ref{SVRformula}), for more energetic wave, with faster bottom orbital velocity, the transport magnitude will get larger.\\

%The wave-current interaction's effect on sediment transport. (Schematic diagram 2) (100)
At the tidal inlet, the surface gravity wave is always heading onto the shore, while the current is varying its direction during a tidal cycle. The local current velocity magnitude gradient $\partial |U|/\partial l$ following the wave propagating direction $\hat{l}$ will be determined by the bathymetry constraint, which will not change its sign during a tidal cycle. This means if a local wave package is squeezed or stretched at flood tide, it will be stretched or squeezed at ebb tide. As the sediment transport in (\ref{SVRformula}) is always following the current direction, the tidal averaged net effect of the wave-current interaction will increase either sediment erosion or accretion at a specific location. Figure (\ref{Wave-Current}) illustrates such current-wave interactive impact on local sediment transport schematically.\\ 

\begin{figure}
  \centerline{\includegraphics[scale=0.8]{schematic1}}
  \caption{This schematic diagram shows the local wave-induced sediment transport $q_{w}$ versus the local current velocity profile $U$. The blue line shows the surface wave profile. $c$ is the wave phase speed, and the arrow above it shows its propagation direction.}
  \label{Wave-Current}
\end{figure}

%The wave-bathymetry interaction's effect on sediment transport. (Schematic diagram 3) (100)
Also, at the Old Inlet Breach, the bathymetry varies a lot. From very recent bathymetry research, the shallowest area is at around $1.5m$ and the deepest area can be up to $5m$. So, there will be significant local depth gradient to change the phase velocity of the wave. According to (\ref{WS1}) and (\ref{WS2}), positive or negative depth gradient along the wave propagation path will stretch or squeeze the wave profile. Since the bathymetry gradient will almost not change during one tidal cycle, for positive depth gradient, the wave will promote sediment erosion during the ebb tide, and sediment accretion during the flood tide. It seems that such process would compensate with each other after tidal average, however, if we consider the change of the coefficient $(\sqrt{gh}+U)^{-1}$ in equation (\ref{WS1}) and (\ref{WS2}), which will be large at ebb phase and small at the flood phase,  we will have the net sediment erosion for $\partial h/\partial l>0$; and net sediment accretion for $\partial h/\partial l<0$. Figure (\ref{Wave-Bathymetry}) illustrates such bathymetry-wave interactive effect on local sediment transport schematically.\\

\begin{figure}
  \centerline{\includegraphics[scale=0.8]{schematic2}}
  \caption{This schematic diagram shows the local wave-induced sediment transport $q_{w}$ versus the local bathymetry. The blue line shows the surface wave profile.  $c$ is the wave phase speed, and the arrow above it shows its propagation direction.}
  \label{Wave-Bathymetry}
\end{figure}


%Sum up the wave-current effect and wave-bathymetry effect. (50)
Combining (\ref{WS1}) and (\ref{WS2}), the interaction with the wave and current or bathymetry will act simultaneously at one location, together with local current gradient asymmetry during a tidal cycle, we should have a complete understanding of the local sediment transport mechanism at the tidal inlet.\\

%Something about storm. (100)
Finally, during the extreme weather like a winter storm, the offshore surface gravity wave grows dramatically, and the wave-induced effect will become important. Dodet et al. proposed in their work in \cite{dodet2013wave} that wave-induced seaward sediment transport attenuation will become significant. Because at ebbing phase, the current and bathymetry induced wave breaking became more fierce, which increases the asymmetry of the wave field during a tidal period. Also, in the same sense, the wave-current interaction induced local effect we propose here will be more prominent. As from the aerial pictures (\ref{Aerial}), it is unclear whether the majority contribution is from independent storm events, or from a long time cumulative effect. So, is necessary to test our proposed sediment transport process during a storm event and a mild event to make a comparison.\\


\section{Objectives}
There are three primary objectives for the proposed research:
\begin{itemize}
  \item Investigate the major driving factors to the sediment transport through the Old Inlet Breach into the Great South Bay.
  \item Investigate how wave-current-bathymetry interaction at the Old Inlet Breach will contribute to the local sediment transport (erosion and accretion).
  \item Investigate the sediment transport at the Old Inlet Breach during a storm event.
\end{itemize}

\section{Hypothesis}
For the first primary objective, we hypothesize that the wave and tidal climate will continuously pump sediment into the back bay at the Old Inlet Breach, there will be three significant effects contributing to such process positively:
\begin{itemize}
  \item The tidal stage variation will contribute to the pumping process by creating a local M4 tidal constitute at the inlet region. Offshore M4 tidal constitutes will also contribute positively to the pumping effect.
  \item The tidal stage variation and local hydrodynamic nonlinearity will contribute to sediment pumping effect positively.
  \item Surface gravity wave will increase the sediment transport asymmetry and boost the sediment transport at flood direction.

\end{itemize}

For the second primary objective, we hypothesize that the surface gravity wave will act with current gradient and bathymetry to create tidally averaged local sediment transport divergence or convergence:

\begin{itemize}
\item If we only consider wave-current interaction (with no wave breaking), such interaction will lead to net sediment divergence and convergence locally.
\item If we only consider wave bathymetry interaction, such interaction will also lead to net sediment divergence and convergence locally.
\end{itemize}

For the third primary objective, we hypothesize that during a storm event the sediment transport will be increased dramatically:

\begin{itemize}
\item The overall sediment transport will increase comparing that during with mild weather.
\item The wave induced sediment transport and local erosion/accretion effect will become more significant.
\end{itemize}
 
 


\section{Research Approach}
\subsection{Field work}
%Measuring wave-current-bathymetry interaction from RTK-GPS data (300)
\subsubsection{Measuring local wave field}
%%Basic introduction about RTK-GPS method (50)
The Real-time Kinematics Global Positioning System (RTK-GPS) gives precise horizontal position measurement, and simultaneous vertical measurement from the bottom to the transducer mounted on a ship. Usually, such system is used to produce the bathymetry mapping. We here propose a method to estimate the wave field measurement from these data.\\

\begin{figure}
  \centerline{\includegraphics[scale=0.4]{TM}}
  \caption{The map of selected traces on from one survey as an example.}
  \label{Tracemap}
\end{figure} 

%%Describing the method to measure wave from RTK_GPS data (250)
\noindent To get useful information from the RTK-GPS measurement. The ship traces are specially selected (see figure \ref{Tracemap}). Only the data measured from those traces that are at the direction of going across the main channel is used. Because of the boundary constraining, the current velocity $\mathbf{U}$ and wave vector $\mathbf{k}$ will point along the main channel, so that we can expect the wave vector to be heading perpendicular to ship velocity $\mathbf{V}$. As the observed wave angular frequency $\omega$ estimated from the coordinate moving with the ship velocity $\mathbf{V}$ is defined by:

\begin{align}
\omega=\Omega+\mathbf{k}\cdot\mathbf{U}-\mathbf{k}\cdot\mathbf{V},
\label{DSo}
\end{align}

\noindent where $\Omega$ is the intrinsic wave angular frequency, and the following terms are the Doppler shift caused by current and ship motion. If the $\omega$ is estimated on those traces we have specially selected, we can neglect the last term in equation (\ref{DSo}). Moreover, in such case, $\mathbf{k}$ is roughly parallel to $\mathbf{U}$, (\ref{DSo}) can be further approximated by:

\begin{align}
\omega=\Omega+kU.
\label{DSs}
\end{align}

\noindent The observed wave angular frequency $\omega$ and wave height $H$ can be obtained directly from statistics \cite{komen1996dynamics}. The depth averaged current velocity $U$ will be got from a validated current model output, which will be described below. And the intrinsic wave frequency is estimated by $\Omega=k\sqrt{gh}$ at the shallow water limit. Then, the local wavenumber $k$ can be got by directly applying equation (\ref{DSs}). In this way, we will be able to measure the wave field along the main channel.\\

%The example output about the omega along the inlet (200)

From a preliminary testing of this method, the trend of observed wave frequency $f_{d}=\omega/(2\pi)$ is compared with the average water depth along the trace (see in the left panel in figure (\ref{CR})). We can see that the trend of the frequency follows the changing of bathymetry. Such relation can be understood through equation \ref{DSs}. As the intrinsic wave frequency is a constant, and $k=\Omega/\sqrt{gh}$, increasing depth indicates decreasing $k$, which will lead to the decrease of $f_{d}$. Also, some contribution might come from the changing current speed $U$ along the main channel, it might be able to explain the local anomaly at $4^{\rm{th}}$, $10^{\rm{th}}$ and $11^{\rm{th}}$ transects. To test the sensitivity of the method, the trend of observed wave frequency is compared with direction cosine of the trace. No significant covariant pattern is found, which means the trend of observed wave frequency is not sensitive to the direction fluctuation of the selected trace. Of course, more careful analysis need to be done to evaluate these data.\\

\begin{figure}
  \centerline{\includegraphics[scale=0.4]{CR}}
  \caption{The left panel gives the trend comparison of $f_{d}$ vs. depth, the right panel gives the trend comparison of $f_{d}$ vs. the direction cosine of the selected traces.}
  \label{CR}
\end{figure} 


%Local wave measuring cruise (200) (Put in the map showing the position to put the adcp)
\subsubsection{Local wave measurement cruise}
Two Acoustic Doppler Current Profilers (ADCP) will be deployed at the Old Inlet breach to measure the wave and current on the ebb-shoal and inside the main channel. As shown in figure \ref{Deploymap}, we will place one ADCP on the edge of the ebb-shoal at around $3m$ water depth; the other ADCP will be placed inside the main channel at around $5m$ water depth. Both ADCPs will keep taking a $2Hz$ measurement of the local vertical velocity profile and pressure for $72$ hours, which is about $6$ tidal cycles. The vertical range will be set to be $6m$, and the vertical cell size will be set to be $0.3m$. To prevent the apparatus from being buried by the sediment, we will choose relatively mild weather situation for this cruise.\\
  
\begin{figure}
  \centerline{\includegraphics[scale=0.5]{MAP}}
  \caption{The Map of planned current and wave measurement cruise. Red dots show the position scheduled for ADCP deployment. The green triangle shows the sediment sampling position.}
  \label{Deploymap}
\end{figure}  
  
The current velocity profile is obtained by averaging the velocity time series in every 20 minutes. At the same time, the periodic component of the time series will be Fourier transformed into time-varying spectrum. The significant wave height $H_{s}$ and the dominant wave period $T_{p}$ time series are got directly from spectrum series. The Doppler shift will be counted in by applying $(\omega-U)^2=gh$, where $U$ is the depth-averaged current velocity.\\

The $H_{s}$, $T_{p}$ and $U$ time series will be used to validate the wave-meanflow coupled hydrodynamic model mentioned below.\\

\subsection{Modeling}
\subsubsection{Hydrodynamic Modeling}
%Numerical Model setting up description (200)
%%General description on the Current setting and wave setting (100)
The modeling of the hydrodynamics at the Old Inlet Breach contains two parts. The Finite Volume Coastal Ocean Model (FVCOM, \cite{chen2012unstructured}) will produce the current field, and the third generation model Simulating Waves Nearshore (SWAN) will generate the wave field \cite{booij1999third}. In the proposed work, we will implement a one-way coupling, i.e., the current model will not contain wave forcing, but the current field will force the wave. The current model will be directly based on previous work \cite{hinrichs2018assessment}. The model grid of the wave will be extended offshore to $\sim70m$, with the offshore resolution at $1000m$. The resolution will increase gradually towards the shore. At the Old Inlet Breach, including the ebb-shoal, main channel, and flood shoal region, the resolution will be at around $1m$.\\ 

%%FVCOM/SWAN Forcing (Bathymetry/Wind/Tide/Offshorewave) (100)
Both wave and current model will apply the most recent bathymetry survey data near the Old Inlet Breach, to match the dynamical situation closest to that during the ADCP survey. We will keep the original current model setup, with the tidal forcing changed to the time interval when the ADCP is being deployed. The wave model will be forced by the spatially varying wave spectrum from an offshore wave model Wave Watch III (WWIII). The wind forcing will also come from WWIII archived forcing data. SWAN will be run at non-stationary mode. The time-varying current field and sea level output from the FVCOM will be interpolated onto the SWAN grid at each time step.\\

%Numerical Model validation (50)
The ADCP survey data time series will be used to validate the model. For the current part, the $20min$ averaged current will be used for comparing with the ADCP measurement. For the wave part, local significant wave height, dominant wave period, dominant wave direction time series will be used for comparison. \\

%Numerical Model output (50)
After validation, the current field $U$ and the wave field will be substitute into the Soulsby-Van-Rijn formula (\ref{SVRformula}) to calculate the local sediment transport flux. To be compared with the data from hydraulic modeling, the sediment transport flux filed will be averaged at cross-channel direction. Two different time resolution will be considered: the $20min$ averaged version, which matches the observation data; and the tidal averaged version, which shows long-term transport behavior.\\

%Stormy event modelling (50)
Since the cruise will be deployed during mild weather situation, the validated model mentioned above will not cover any storm event. We will then pick out a forcing scenario with storm event and rerun the model to get the current and wave field during storm event. Again, Soulsby-Van-Rijn formula (\ref{SVRformula}) will be applied to give the sediment transport flux. Then, the comparison with the situation without storm will be done.\\

\subsubsection{Hydraulic Modeling}
%Analytical Modeling (100)
The hydraulic model will be established based on the multi-inlet hydraulic system discussed in previous work \cite{yu2018hydraulic}. However, apart from directly solving numerical ODE solution, a perturbative semi-analytical approach will be adapted to segregate the offshore M4 constitute forcing with the M4 constituent generated by tidal stage variation. The non-linearity induced aperiodic volume transport will also be distinguished with the periodic part in such way. Efforts will be made to let the hydraulic equation adapt the case with strong current velocity gradient, which is typical for Old Inlet Breach \cite{hinrichs2018assessment}; and couple wave's dynamics. The ocean tide level $\eta_{0}$ for both storm event or mild weather situation during the cruise will be used to force the model.\\

%Analytical Model validation (50)
The hydraulic model will output the inlet water level $\eta_{m}$ and mean current velocity $u$ time series. They will be compared with the integrated, validated hydrodynamic model output for its own validation.\\

%Analytical Model output (50)
The validated $u$ and inlet averaged wave field will be substituted into the Soulsby-Van-Rijn formula (\ref{SVRformula}) to calculate the mean sediment transport flux. The contribution of the M4 tide; net volume transport; and wave induced tidal asymmetry will be evaluated separately.\\



\subsection{Hypothesis Testing}
%%A centence about the general starting point of hypothesis testing (30)
The hypothesis we made above will be tested by the data obtained from field study and the validated models introduced above:

\begin{itemize}
%Hypothesis validation (50)
\item For those hypothesis relate to our first primary objective, they will be tested by the hydraulic model result. If the output indicates that all of the three effects stated, including the M4 tide, net volume transport, and surface gravity wave induced asymmetry, will generate a sediment transport through the old inlet breach into the back bay, the original hypothesis is verified. 
%Hypothesis validation (50)
\item For those hypothesis relate to our second primary objective, they will be tested by the RTK-GPS data analysis result and hydrodynamic model output. The model output and field data will be substituted into local wave number balance equation (\ref{WS1}), and local wave energy balance equation (\ref{WS2}). If those balance stays, the original hypothesis is verified.

\item For those hypothesis related to our third primary objective, they will be tested by comparing the hydraulic model or hydrodynamic model in mild weather and in the storm event. If the model results in storm event is showing significantly larger sediment transport signal and wave induced sediment transport asymmetry than those result for mild weather, the original hypothesis is verified.
\end{itemize}

\section{Appendix}

\subsection{The energy correction coefficient $m$}\label{Corisec}
Since the hydraulic equation (\ref{Hydroeqn1}) and (\ref{Hydroeqn2}) is got from the integral of the Bernoulli's equation of micro flow tube along the topography, the integral energy flux $\iint \rho v^{3}ds$ at the cross-section does not equal to the average energy flux $\rho u^{3}$ times the cross section area $A$. Here $u=\iint vds/A$. To write out the total hydraulic head, a parameter $m$ is introduced:

\begin{align}
m=\frac{\iint\rho v^{3}ds}{\rho u^{3}A}.
\end{align}

Such coefficient is introduced by Coriolis in 1836 \cite{coriolis1836etablissement}, and its value is always larger than 1 but rarely exceed 1.15, practically \cite{li1991correction}. For uniformly distributed flow at cross section, $m=1$.

\subsection{M2-M4 interaction}\label{M2M4intersec}

%The general introduction of M2-M4 interaction (200)
The coupling of M2 and its over-tide M4 will cause tidal asymmetry. The relative amplitude determines $a_{M4}/a_{M2}$ the strength of tidal asymmetry; the more comparable the amplitude is, the stronger the asymmetry is. The configuration of asymmetry is determined by $\delta\phi_{M2/M4}=2\phi_{M2}-\phi_{M4}$. Figure (\ref{M2M4inter}) shows eight different configurations for eight different relative phase. The tidal signal on $y$ axis can be defined to be either current velocity or water elevation. If the coupling is written in sine function, like in equation (\ref{STesti}), there are two different coupling phases. In the first phase, when $\delta\phi_{M2/M4}\in[0,\pi)$, the positive amplitude exceed the negative amplitude；while in the second phase, when $\delta\phi_{M2/M4}\in[\pi,2\pi)$, the negative amplitude exceed the positive one. During the first phase, the duration of the positive signal is shorter than the negative signal; during the second phase, the duration of the negative signal is shorter.\\

Specifically, for the current velocity time series, we found from analyzing the FVCOM output at the Old Inlet Breach. With the $\delta\phi_{M2/M4}^{C}=168^{\circ}$, it falls into the configuration shown in the fourth panel in the figure (\ref{M2M4inter}). This case is a typical flooding dominant case, with the flooding velocity amplitude exceeds the ebbing velocity amplitude, and the flooding phase is shorter than the ebbing phase.\\

\begin{figure}
  \centerline{\includegraphics[scale=0.6]{sinbM2M4inter}}
  \caption{These panels show the configurations of each characteristic $\delta\phi_{M2/M4}^{C}$.}
  \label{M2M4inter}
\end{figure} 
 

\bibliography{Citation2.bib}
\bibliographystyle{plain}



% --------------------------------------------------------------
%     You don't have to mess with anything below this line.
% --------------------------------------------------------------
 
\end{document}